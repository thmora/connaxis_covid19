import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:connaxis_covid19/src/app/app.dart';
import 'package:connaxis_covid19/src/services/data/data_provider.dart';
import 'package:connaxis_covid19/src/services/notifications/push_notifications_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  final pushProvider = PushNotificationsProvider();
  await pushProvider.initNotifications();
  runApp(
    ChangeNotifierProvider(create: (_) => DataProvider(), child: App()),
  );
}
