import 'package:flutter/material.dart';

class MyColors {
  static final Color scaffoldBg = Color(0xFF443b95);
  static final Color tabBg = Color(0xFF665db0);

  static final Color green = Color(0xFF4d9852);
  static final Color blue = Color(0xFF3487d7);
  static final Color amber = Color(0xFFf9b751);
  static final Color red = Color(0xFFf45449);
  static final Color white = Color(0xFFf0f0f0);
}

final ThemeData theme = ThemeData(
  scaffoldBackgroundColor: MyColors.scaffoldBg,
  appBarTheme: AppBarTheme(
    backgroundColor: Colors.transparent,
    elevation: 0,
    actionsIconTheme: IconThemeData(color: Colors.white),
  ),
  textTheme: TextTheme(
    headline1: TextStyle(
        color: Colors.white, fontSize: 25, fontWeight: FontWeight.w500),
    headline2: TextStyle(
        color: Colors.white, fontSize: 22, fontWeight: FontWeight.w400),
  ),
  tabBarTheme: TabBarTheme(
    unselectedLabelColor: MyColors.tabBg,
    labelStyle: TextStyle(color: Colors.black),
    unselectedLabelStyle: TextStyle(color: Colors.white),
    labelColor: Colors.white,
  ),
);

class CustomButtonTheme {
  static Color getSelectedColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
    };
    if (states.any(interactiveStates.contains)) {
      return MyColors.white;
    } else {
      return MyColors.white;
    }
  }

  static Color getUnselectedColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
    };
    if (states.any(interactiveStates.contains)) {
      return MyColors.tabBg;
    } else {
      return MyColors.tabBg;
    }
  }

  static Color getSelectedBottomColor(Set<MaterialState> states) {
    return MyColors.white;
  }

  static Color getUnselectedBottomColor(Set<MaterialState> states) {
    return MyColors.tabBg;
  }

  static Color getButtonColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
    };
    if (states.any(interactiveStates.contains)) {
      return MyColors.tabBg;
    } else {
      return MyColors.scaffoldBg;
    }
  }

  static EdgeInsets getPadding(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
    };
    if (states.any(interactiveStates.contains)) {
      return EdgeInsets.symmetric(vertical: 16, horizontal: 64);
    } else {
      return EdgeInsets.symmetric(vertical: 16, horizontal: 64);
    }
  }

  static OutlinedBorder getShape(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
    };
    if (states.any(interactiveStates.contains)) {
      return RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(16.0)),
      );
    } else {
      return RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(16.0)),
      );
    }
  }

  static EdgeInsets getButtonPadding(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
    };
    if (states.any(interactiveStates.contains)) {
      return EdgeInsets.symmetric(vertical: 16, horizontal: 24);
    } else {
      return EdgeInsets.symmetric(vertical: 16, horizontal: 24);
    }
  }

 
}

final ButtonStyle selectedTabStyle = ButtonStyle(
  shape: MaterialStateProperty.resolveWith(CustomButtonTheme.getShape),
  padding: MaterialStateProperty.resolveWith(CustomButtonTheme.getPadding),
  backgroundColor:
      MaterialStateProperty.resolveWith(CustomButtonTheme.getSelectedColor),
);

final ButtonStyle unselectedTabStyle = ButtonStyle(
  shape: MaterialStateProperty.resolveWith(CustomButtonTheme.getShape),
  padding: MaterialStateProperty.resolveWith(CustomButtonTheme.getPadding),
  backgroundColor:
      MaterialStateProperty.resolveWith(CustomButtonTheme.getUnselectedColor),
);

 
final ButtonStyle textButtonStyle = ButtonStyle(
  shape: MaterialStateProperty.resolveWith(CustomButtonTheme.getShape),
  padding:
      MaterialStateProperty.resolveWith(CustomButtonTheme.getButtonPadding),
  backgroundColor:
      MaterialStateProperty.resolveWith(CustomButtonTheme.getButtonColor),
);

final ButtonStyle elevatedButtonStyle = ButtonStyle(
  shape: MaterialStateProperty.resolveWith(CustomButtonTheme.getShape),
  padding:
      MaterialStateProperty.resolveWith(CustomButtonTheme.getButtonPadding),
  backgroundColor:
      MaterialStateProperty.resolveWith(CustomButtonTheme.getSelectedColor),
);
