import 'package:connaxis_covid19/src/pages/home/widgets/report_new_cases_dialog.dart';
import 'package:flutter/material.dart';
import 'package:connaxis_covid19/src/pages/home/widgets/data_box.dart';
import 'package:connaxis_covid19/src/pages/home/widgets/selectable_button.dart';
import 'package:connaxis_covid19/src/utils/styles.dart';
import 'package:provider/provider.dart';
import 'package:connaxis_covid19/src/services/data/data_provider.dart';

class HomeBody extends StatefulWidget {
  @override
  _HomeBodyState createState() => _HomeBodyState();
}

enum TabSelect { pais, mundo }

enum BottomSelect { dia, semana, mes }

class _HomeBodyState extends State<HomeBody> {
  TabSelect tabSelect = TabSelect.pais;
  BottomSelect bottomSelect = BottomSelect.dia;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, bc) {
        return Center(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Estadisticas Covid-19",
                  style: Theme.of(context).textTheme.headline1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Casos en:",
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
              Container(
                width: bc.maxWidth * .9,
                height: bc.maxHeight * .08,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: MyColors.tabBg,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SelectableButton(
                      onPressed: () => tabSelectionsPressed(TabSelect.pais),
                      text: "Mi país",
                      selected: tabSelect == TabSelect.pais,
                      selectedStyle: selectedTabStyle,
                      unselectedStyle: unselectedTabStyle,
                    ),
                    SelectableButton(
                      onPressed: () => tabSelectionsPressed(TabSelect.mundo),
                      text: "Todo el mundo",
                      selected: tabSelect == TabSelect.mundo,
                      selectedStyle: selectedTabStyle,
                      unselectedStyle: unselectedTabStyle,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(24.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _button("DIA", BottomSelect.dia),
                    _button("SEMANA", BottomSelect.semana),
                    _button("MES", BottomSelect.mes),
                  ],
                ),
              ),
              DataBox(bc),
              Padding(
                padding: const EdgeInsets.only(top: 64.0),
                child: TextButton(
                  style: elevatedButtonStyle,
                  onPressed: () => reportNewCasesDialog(context),
                  child: Text(
                    "Reportar Nuevos Casos",
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  void tabSelectionsPressed(TabSelect select) {
    setState(() {
      tabSelect = select;
    });
    updateData();
  }

  void bottomSelectionsPressed(BottomSelect select) {
    setState(() {
      bottomSelect = select;
    });
    updateData();
  }

  void updateData() =>
      Provider.of<DataProvider>(context, listen: false).updateData();

  Widget _button(String text, BottomSelect select) {
    return TextButton(
      style: textButtonStyle,
      onPressed: () => bottomSelectionsPressed(select),
      child: Text(text, style: TextStyle(fontSize: 20, color: Colors.white)),
    );
  }
}
