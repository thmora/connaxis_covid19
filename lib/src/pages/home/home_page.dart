import 'package:connaxis_covid19/src/pages/home/home_body.dart';
import 'package:connaxis_covid19/src/services/data/data_provider.dart';
import 'package:connaxis_covid19/src/services/notifications/push_notifications_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final pushProvider = PushNotificationsProvider();

  @override
  void initState() {
    super.initState();
    pushProvider.initNotifications();
    pushProvider.mensajesStream.listen((notif) {
      print('notificacion recibida: $notif');
      Provider.of<DataProvider>(context, listen: false).addNotification(notif);
    });
  }

  @override
  void dispose() {
    pushProvider.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final notifs = Provider.of<DataProvider>(context).notifs;
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: Icon(
              notifs.isEmpty
                  ? Icons.notifications_outlined
                  : Icons.notifications_active_outlined,
            ),
            onPressed: () {},
          )
        ],
      ),
      body: HomeBody(),
    );
  }
}
