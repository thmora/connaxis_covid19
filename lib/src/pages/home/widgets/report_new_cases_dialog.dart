import 'dart:io';

import 'package:connaxis_covid19/src/services/data/data_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

reportNewCasesDialog(BuildContext context) {
  final TextEditingController _cant = TextEditingController();

  _updateCases() {
    Navigator.pop(context);
    Provider.of<DataProvider>(context, listen: false).agregarCasos(int.parse(_cant.text));
  }

  if (Platform.isAndroid) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Reportar nueva cantidad de casos"),
          content: TextField(
            controller: _cant,
            keyboardType: TextInputType.number,
          ),
          actions: [
            MaterialButton(
              child: Text("Reportar"),
              elevation: 5,
              textColor: Colors.blue,
              onPressed: () => _updateCases(),
            )
          ],
        );
      },
    );
  } else {
    showCupertinoDialog(
      context: context,
      builder: (_) {
        return CupertinoAlertDialog(
          title: Text("Reportar nueva cantidad de casos:"),
          content: CupertinoTextField(
            controller: _cant,
            keyboardType: TextInputType.number,
          ),
          actions: [
            CupertinoDialogAction(
              child: Text("Reportar"),
              isDefaultAction: true,
              onPressed: () => _updateCases(),
            ),
            CupertinoDialogAction(
              child: Text("Volver"),
              isDestructiveAction: true,
              onPressed: () => Navigator.pop(context),
            ),
          ],
        );
      },
    );
  }
}
