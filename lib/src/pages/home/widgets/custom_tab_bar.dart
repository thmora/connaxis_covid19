import 'package:connaxis_covid19/src/utils/styles.dart';
import 'package:flutter/material.dart';

class CustomTabBar extends StatelessWidget {
  CustomTabBar(this.bc);
  final BoxConstraints bc;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: bc.maxWidth * .8,
      height: bc.maxHeight * .1,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: MyColors.tabBg,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextButton(onPressed: () {}, child: Text("Mi Pais")),
          TextButton(onPressed: () {}, child: Text("Todo el mundo")),
        ],
      ),
    );
  }
}
