import 'package:connaxis_covid19/src/services/data/data_provider.dart';
import 'package:flutter/material.dart';
import 'package:connaxis_covid19/src/pages/home/widgets/data_container.dart';
import 'package:connaxis_covid19/src/utils/styles.dart';
import 'package:connaxis_covid19/src/services/extensions/int_ext.dart';
import 'package:provider/provider.dart';

class DataBox extends StatelessWidget {
  DataBox(this.bc);
  final BoxConstraints bc;

  @override
  Widget build(BuildContext context) {
    double bottomRowWidth = bc.maxWidth * .3;
    double rowHeight = bc.maxHeight * .125;
    final data = Provider.of<DataProvider>(context).data;
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              DataContainer(
                height: rowHeight,
                width: bc.maxWidth * .45,
                text: "Infectados",
                number: data["Infectados"]!.formated,
                color: _getColor(data["Infectados"]!),
              ),
              DataContainer(
                height: rowHeight,
                width: bc.maxWidth * .45,
                text: "Fallecidos",
                number: data["Fallecidos"]!.formated,
                color: MyColors.red,
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              DataContainer(
                height: rowHeight,
                width: bottomRowWidth,
                text: "Recuperados",
                number: data["Recuperados"]!.formated,
                color: MyColors.green,
              ),
              DataContainer(
                height: rowHeight,
                width: bottomRowWidth,
                text: "Activos",
                number: data["Activos"]!.formated,
                color: MyColors.blue,
              ),
              DataContainer(
                height: rowHeight,
                width: bottomRowWidth,
                text: "Graves",
                number: data["Graves"]!.formated,
                color: MyColors.amber,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Color _getColor(int number) {
    Color color = MyColors.red;
    if (number < 50000) {
      color = MyColors.green;
    } else if (number > 50000 && number < 150000) {
      color = MyColors.amber;
    }
    return color;
  }
}
