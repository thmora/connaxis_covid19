import 'package:connaxis_covid19/src/utils/styles.dart';
import 'package:flutter/material.dart';

class SelectableButton extends StatelessWidget {
  SelectableButton({
    this.text = "",
    this.onPressed,
    this.selected = false,
    this.selectedStyle,
    this.unselectedStyle,
  });
  final String text;
  final void Function()? onPressed;
  final bool selected;
  final ButtonStyle? selectedStyle;
  final ButtonStyle? unselectedStyle;
  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: selected ? selectedTabStyle : unselectedTabStyle,
      onPressed: onPressed,
      child: Text(
        text,
        style: TextStyle(color: selected ? Colors.black : Colors.white),
      ),
    );
  }
}
