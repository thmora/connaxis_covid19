import 'package:flutter/material.dart';

class DataContainer extends StatelessWidget {
  DataContainer({
    this.text = "",
    this.number = "",
    this.width = 0.0,
    this.height = 0.0,
    this.color,
  });
  final String text;
  final String number;
  final double width;
  final double height;
  final Color? color;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
      ),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(text, style: TextStyle(fontSize: 19, fontWeight: FontWeight.w500)),
            Text(
              "$number",
              style: TextStyle(
                fontSize: 19,
                color: color,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
