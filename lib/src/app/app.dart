import 'package:connaxis_covid19/src/pages/home/home_page.dart';
import 'package:connaxis_covid19/src/utils/styles.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Estadisticas Covid-19',
      theme: theme,
      routes: {
        "/home": (_) => HomePage(),
      },
      initialRoute: "/home",
    );
  }
}
