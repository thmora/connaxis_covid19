extension IntExtension on int {
  String get formated {
    String formated = this.toString();

    if (formated.length > 3) {
      formated = formated.substring(0, formated.length - 3) +
          "," +
          formated.substring(formated.length - 3, formated.length);
      if (formated.length > 7) {
        formated = formated.substring(0, formated.length - 7) +
            "," +
            formated.substring(formated.length - 7, formated.length);
      }
    }
    return formated;
  }
}
