import 'dart:async';
import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationsProvider {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  final _mensajesStreamController = StreamController<String>.broadcast();

  Stream<String> get mensajesStream => _mensajesStreamController.stream;

  Future<dynamic> onBackgroundMessage(RemoteMessage message) async {
    print('====== onMessage ====== ');
    print('message: $message');
    String notif = 'no-data';

    notif = message.data["cuerpo"] ?? 'no-data';

    _mensajesStreamController.sink.add(notif);
  }

  initNotifications() async {
    await _firebaseMessaging.requestPermission();
    final token = await _firebaseMessaging.getToken();

    print('==== FCM Token ======');
    print(token);

    FirebaseMessaging.onMessage.listen((event) {
      onMessage(event);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((event) {
      onOpenedApp(event);
    });
    /*  FirebaseMessaging.onBackgroundMessage(
        (message) => onBackgroundMessage(message)); */
  }

  Future<dynamic> onMessage(RemoteMessage message) async {
    print('====== onMessage ====== ');
    print('message: ${message.data}');
    String notif = 'no-data';

    notif = message.data["cuerpo"] ?? 'no-data';

    print(notif);

    _mensajesStreamController.sink.add(notif);
  }

  Future<dynamic> onOpenedApp(RemoteMessage message) async {
    print('====== onOpenedApp ====== ');
    print('message: ${message.data}');

    String notif = 'no-data';

    notif = message.data["cuerpo"] ?? 'no-data';

    print(notif);

    _mensajesStreamController.sink.add(notif);
  }

  dispose() {
    _mensajesStreamController.close();
  }
}
