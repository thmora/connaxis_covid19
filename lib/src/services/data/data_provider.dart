import 'package:flutter/material.dart';
import 'dart:math';

class DataProvider with ChangeNotifier {
  Map<String, int> _data = {
    "Infectados": _randomNumber(),
    "Fallecidos": _randomNumber(),
    "Recuperados": _randomNumber(),
    "Activos": _randomNumber(),
    "Graves": _randomNumber(),
  };
  Map<String, int> get data => this._data;

  List _notifs = [];
  List get notifs => this._notifs;

  void addNotification(String notification) async {
    _notifs.add(notification);
    notifyListeners();
  }

  set data(Map<String, int> value) {
    _data = value;
    notifyListeners();
  }

  static int _randomNumber() {
    Random random = new Random();
    int randomNumber = random.nextInt(1100000);
    return randomNumber;
  }

  void updateData() {
    Map<String, int> newData = {
      "Infectados": _randomNumber(),
      "Fallecidos": _randomNumber(),
      "Recuperados": _randomNumber(),
      "Activos": _randomNumber(),
      "Graves": _randomNumber(),
    };
    data = newData;
  }

  void agregarCasos(int cant) {
    data["Activos"] = data["Activos"]! + cant;
    data["Infectados"] = data["Infectados"]! + cant;
    notifyListeners();
  }
}
